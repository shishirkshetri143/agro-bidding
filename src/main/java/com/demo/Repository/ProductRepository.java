package com.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.demo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
	Product  findByPname(String Pname);

}
